# Learning C++

Learning something new is the easiest when you can immediately make use of the new knowledge gained.
In this guide we'll try to finish the boring basics as quickly as possible, so you can continue learning in a project that you care about.

This will be easier to complete if you already know about concepts of programming, but it should be possible to work through it otherwise by looking up concepts you don't know already.

# Basic Setup

In order to make learning a little easier, we'll make use of an integrated development environment that will already tell you about syntax errors and other issues while you are writing.
For this we'll use QtCreator, which you can download [here](https://www.qt.io/download-qt-installer) in combination with the rest of the Qt framework which we can maybe use later.

After QtCreator is installed, start it and click "New" to create a new project.
Then select "Project without Qt", and then "pure C++ application".
You can leave the other options to their default values.

Your project will start with something like the following:
```
#include <iostream>

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    return 0;
}
```

This code will just print "Hello World" to the console, which is not of much use to us right now, so you can remove everything between the two curly braces.

# Basic concepts

<!-- C++ is a language that combines aspects of object-oriented programming and functional programming. -->

## Variables

We'll start by creating a variable.
A variable can store data of a specific type, like a number, a string of characters or other more advanced types.
In C++, every variable needs to have a fixed type which can't be changed anymore.

The basic syntax for a variable is
```C++
Type variable_name;
```
It is important to note that every statement ends with a semicolon.

There are a number of basic types on which all other types are based.

The ones most interesting in the beginning are:
| Use                                        | type name in C++  |
|--------------------------------------------|-------------------|
| An integer number                          | int               |
| A floating point number                    | float             |
| A boolean value (either `true` or `false`) | bool              |

A variable can be initialized with a value like this:
```
int number = 1;
```

It is not neccessary to specify the type every time you create a variable. If it is already clear from the context which type a variable has, you can use the `auto` type.
`auto` is not really a type, but a placeholder for a type that the compiler is yet to find out.
When assigning a number like in our example, the compiler would choose `int` as a type automatically.

```
auto number = 1;
```

In this case, `auto` expands to `int`, as above.
To make the code easier to understand later, it sometimes still makes sense to specify the type even if it's clear already.

## Operators

C++ has the concept of operators, which allows doing specific operations with one or two variables.
Basic types support a number of arithmethic operators, like `+`, `-`, `*` (multiply), `/` (divide) and some more.

If we reuse the line of code from the beginning, we can do something with our variable, like multiplying it by 2:
```C++
int number = 1;
int twice_number = number * 2;
```

Some operators can also modify values in place, for example:

```C++
int number = 1;
number += 2;
```
changes the value of `number` to the value of `number + 2`.

## Constants

In many cases, you just need to store a value without having to modify it later on.
In this case it's a good idea to mark your variable as `const`.
This way the compiler throws you an error, when you accidentally change the variable.

```C++
const int seconds_of_a_minute = 60;
```

It is reasonable to assume that this value is never going to change, so let's make sure we don't mistakenly modify it.

# More advanced types

Only the basic types are available by default.
If you want to make use of other types, you need to tell the compiler that.

For example, if you want to store a text, you can make use of the `string` type.
The `string` in C++ is not part of the language itself, but the so called "standard library" (`std`).
It is shipped with every C++ compiler, but we still need to tell the compiler that we want to use it.

You can include it into your current file as follows:
```C++
#include <string>
```

Then you can use it just like we did with the basic types in the beginning:
```C++
std::string hello_world = "Hello World";
```

# Printing to the Console

To make it easier to see what our program is doing, we can print variables to the console.

For that we'll use another part of the standard library, called `iostream`, where `io` stands for "Input / Output".
Variables that support one of the stream operators (`<<` and `>>`) are usually called streams.
Basically this type has no value, but you can continously stuff values into it, which are then processed in some way.
In our case they're printed to the standard output (our console).

```C++
int a = 1;
std::cout << "'a' equals " << a;
```

# Branches (if statements)

As you can see, a continous flow of instructions isn't enough to make a program that can really do something useful.
We also need branches.
For example, if the user inputted an invalid value, we want to do something else than if the value was valid.

A branch does this job and can decide between two different behaviours depending on a condition, a boolean value (`true` or `false`).

The code that should be executed in the cases is delimited by a curly brace.

```C++
bool exists = true;

if (exists) {
    std::cout << "It exists";
} else {
    std::cout << "It doesn't exist"
}
```

# Loops

Loops execute the same code multiple times.
The most basic loop is the `while` loop, which executes code while a condition is matched.

```C++
int counter = 0;
while (counter < 10) {
    counter += 1;
    std::cout << counter << std::endl;
}
```

This code counts up until it has reached 10.

# Functions

Often a specific part the code needs to be used multiple times in a program.
For this reason there are functions.
Functions hold a small part of the code, which should be easy to understand and essentially can be reused whenever needed.

A function can have multiple input variables and has one output variable.
The function can process the values and output the result.

The following code needs two values, and returns an integer.
The returned number is the sum of both values.
```C++
int add(int value1, int value2)
{
    return value1 + value2;
}
```

As you might have noticed, what we wrote in the beginning was also a function, the `main` function.
Functions can call other functions, just like we can call other functions from the main function, which is the entry point to our program.

The following code uses our `add` function and stores the result in a variable.
The variable should now contain 3.
```C++
int sum = add(1, 2);
```

# Vectors and for-Loops

Imagine, we want to ask the user for some values, so we can calculate the average.
With our current skills we are facing a problem here.
We don't know how many values the user will enter and we probably also don't want to define so many variables manually.

Code like this looks poor:
```C++
int value1;
int value2;
int value3;
int ...
```

What we need is called a `vector`.
A `vector` is a type, which can contain as many values as we want.
The following code creates a vector that contains integers.
The type the vector holds is put between `<` and `>`.
This is called a template parameter.
You can create a `vector` of any type, even `std::string`s.

```C++
std::vector<int> temperatures = {21, 32, 30, 27, 29};
int sum = 0;
for (int temperature : temperatures) {
    sum += temperature;
}

std::cout << sum / temperatures.size();
```

It also demonstrates a new loop we didn't previously see, the `for` loop.
The `for` loop can iterate over a sequence like a vector, and run code for each element in it.
In this case, we add the value to a variable defined outside of the loop.

We print the resulting sum divided by the number of elements in the vector.
`size()` is a function too.
If you haven't used object-oriented programming before, you might wonder why it is executed in this way.
Apparently something is special about the vector.
It turns out that the vector is a class and the `size()` function is part of the class.
What that means will be explained in the next chapter.

# Classes

Classes are a kind of blueprints grouping some data and functionality together.
They can be used to define new types like the `std::vector` we just used.

Classes have got attributes and functions that operate on these attributes.
Attributes are basically just variables each object will get.
They are used to store the internal data of an object.

The following class demonstrates the combination of attributes and functions.

```C++
class Tree
{
public:
    int age = 0;
    int height = 0;
    std::vector<int> leaves = {};

    void drop_leaves() {
        leaves.clear();
    };
};
```

Defining a class alone doesn't do anything yet when we launch the program, it is just a blueprint after all.
However, what we can do, is creating variables with type `Tree` now.
These instances of our new class are called objects.
```C++
Tree apple_tree;
apple_tree.age += 1;
apple_tree = {1, 2, {4, 5}};
```

To make it clearer, it is a good style to prefix all members with `m_`.
This is done to avoid name clashes when a function with the same name exists or a function argument with the same name exists.

```C++
class Tree {
public:
    int m_age;
    int m_height;
    std::vector<Leave> m_leaves;
    
    void drop_leaves() {
        m_leaves.clear();
    };
};
```
